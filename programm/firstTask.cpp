#include <iostream>

using namespace std;

void isSimple(int num,bool *simple){
    for(int i=2;i<num && (*simple  = num%i!=0);i++); // тут мы проверяем делится ли наше число на все числа которые иду до него. если оно делится значит не простое
}

int main() {
    int size,input[100], output[100];
    //вводим размер массива до тех пор пока оно не станет такое что нас удовлитворит
    do {
        cout << "enter array size (smaller then 100)" << endl;
        cin >> size;
    } while (size < 1 || size > 100);
    //заполняем масив
    for (int i = 0; i < size; i++) {
        cout << "enter " << i << " item of array" << endl;
        cin >> input[i];
    }
    int counter = 0;
    //проверяем каждое число входного масива и заносим его в выходной в случае если оно не простое
    for(int i=0;i<size;i++) {
        bool  simple;
        isSimple(input[i],&simple);
        if (!simple)output[counter++] = input[i];
    }
    //если все елементы простые вывести соответсвуещее сообщание в противном случае вывести не простые елементы на экран
    if(counter == 0) cout << "all items are simple";
    else for(int i=0;i<counter;i++) cout << "item ["<<i<<"] = "<<output[i]<<endl;
    system("pause");
    return 0;
}
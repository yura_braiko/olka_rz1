#include <iostream>

using namespace std;

int main() {
    int column, row, input[100] [100], buf[100], output[100];
    //узнать размер массива
    do {
        cout << "enter column count (smaller then 100)" << endl;
        cin >> column;
        cout << "enter row count (smaller then 100)" << endl;
        cin >> row;
    } while ((column < 1 || column > 100) && (row < 1 || row > 100));
    //заполнить массив
    for (int i = 0; i < column; i++) {
        for (int j = 0; j < row; j++) {
            cout << "enter element where column = " << i << " and row = " << j << " item of array" << endl;
            cin >> input[i][ j];
        }
    }
    //проинитить буффер
    for (int i = 0; i < row; i++) {
        buf[i] = 0;
    }
    //просумировать элемнеты в колонках
    for (int i = 0; i < column; i++) {
        for (int j = 0; j < row; j++) {
            buf[i] += input[i][ j];
        }
    }
    int max = buf[0];
    //найти максимальные значение
    for (int i = 1; i < column; i++) {
        if (max < buf[i])max = buf[i];
    }

    //в колнках которые содержат максимальную суму вывести минимальный элемент
    for (int i = 0; i < column; i++) {
        if (buf[i] == max) {
            int min = input[i][ 0];
            for (int j = 1; j < row; j++) {
                if (min > input[i][ j]) min = input[i][ j];
            }
            cout << "min item of " << i << " column  = " << min;
        }
    }
    system("pause");
    return 0;
}